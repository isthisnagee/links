---
title: Books of 2019 
date: "2019-07-04"
tags:
  - books
  - books-2019
description: "some books read in 2019"
---


- [Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy ](https://www.goodreads.com/book/show/28186015-weapons-of-math-destruction)
- [Being Mortal: Medicine and What Matters in the End](https://www.goodreads.com/book/show/20696006-being-mortal)
