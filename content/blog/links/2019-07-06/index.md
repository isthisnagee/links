---
title: "Video Game Communities, the Fediverse as a leveler, and Dweets"
description: "winning and losing and dwitter"
date: "2019-07-06"
tags:
  - "video-games"
  - "privilege"
  - "fediverse"
  - "javascript"
---

## javascript

[An analysis of a dweet](http://frankforce.com/?p=6232) posted
on [dwitter](https://dwitter.net), a site where users are
challenged to see what they can create with 140 chars of javascript and a
canvas.

## fediverse, privelege

[An essay by emsenn](https://emsenn.net/essays/there-and-back-again-a-fediverse-journey/) about how popular social media can encode privilege and how the
fediverse can take it away.

## video-games

[A story](https://www.theguardian.com/games/2019/jul/04/how-a-video-game-community-filled-my-nephews-final-days-with-happiness-elite-dangerous) about how great the video game community can be (this one made me tear
up on the BART), and [another](https://comicbook.com/gaming/amp/2019/07/06/super-smash-bros-ultimate-pro-bocchi/) about how the video game community can be pretty
terrible.
