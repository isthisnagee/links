---
title: Employee activism and unions, tabs vs spaces, and dark patterns in shopping websites
date: "2019-07-08"
tags:
  - unions
  - tech
  - a11y
  - design
description: "unionizing, tabs, and dark patterns"
---

## tech, unions

[Employee activism in tech stops short of organizing unions](https://www.nytimes.com/2019/07/08/technology/tech-companies-union-organizing.html).

## a11y

[The real reason to use tabs over
spaces](https://www.reddit.com/r/javascript/comments/c8drjo/nobody_talks_about_the_real_reason_to_use_tabs/).

## design

[Dark Patterns at Scale: Findings from a Crawl of 11K Shopping Websites](https://webtransparency.cs.princeton.edu/dark-patterns/)
