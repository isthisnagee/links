---
title: "Crazy Genius, WMDs, and the US Border Patrol" 
description: "Podcasts, big data, and the war on immigrants"
date: "2019-07-05"
tags:
  - "podcasts"
  - "big-data"
  - "war-on-immigrants"
---

## Podcasts

I've been listening to the [crazy/genius](https://www.theatlantic.com/podcasts/crazygenius/) podcast recently. Season 3 is titled "Unbreak the Internet" and discusses topics like privacy, pornography, influencers, and the surveillance state. I really recommend it.


## Big Data

I recently read [Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy ](https://www.goodreads.com/book/show/28186015-weapons-of-math-destruction), which made it to my list of my [favorite books of 2019](/books/2019). An important look at how a lot of our AI systems encode social biases like race and gender. It defines what makes a system a "WMD" and how systems can make a positive impact. 

## War on Immigrants

[The Intercept archives racists and obscene Facebook posts made by border patrol agents.](https://theintercept.com/2019/07/05/border-patrol-facebook-group/)
